# GitLab CI: Pipeline Syntax: ONLY, and EXCEPT
([Source](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/))

---

Dans son utilisation la plus simple, le only et le except se déclarent comme ceci :

``` yaml
job:only:master:
  script: make deploy
  only:
    - master # Le job sera effectué uniquement lors d’un événement sur la branche master

job:except:master:
  script: make test
  except:
    - master # Le job sera effectué sur toutes les branches lors d’un événement sauf sur la branche master
``` 
	
#### ONLY ET EXCEPT COMPLEX
Dans son utilisation la plus complexe, le only et le except s’utilisent comme ceci :

``` yaml
job:only:master:
  script: make deploy
  only:
    refs:
      - master # Ne se fera uniquement sur master
    kubernetes: active # Kubernetes sera disponible
    variables:
      - $RELEASE == "staging" # On teste si $RELEASE vaut "staging"
      - $STAGING # On teste si $STAGING est défini
```